// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// <-- Non-regression test for bug 540 -->
//
// <-- Bugzilla URL -->
// http://forge.scilab.org/index.php/p/apifun/issues/540/
//
// <-- Short Description -->
//   assert_typecallable does not manage compiled functions.

f = sin;
apifun_checkcallable ( "myalgorithm" , f , "f" , 1 );
