// ====================================================================
// Copyright (C) 2012 - Michael Baudin
// Copyright INRIA 2008
// Allan CORNET
// Simon LIPP
// This file is released into the public domain
// ====================================================================
function apifunBuildHelpEN()
    help_lang_dir = get_absolute_file_path('build_help.sce');
    tbx_build_help(TOOLBOX_TITLE, help_lang_dir);
endfunction
apifunBuildHelpEN();
clear apifunBuildHelpEN
